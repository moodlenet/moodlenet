// @index(['../components/**/*.stories.tsx'], f => `export * as ${f.name.replace('.stories','Stories')} from '${f.path}${f.ext==='.tsx'?'.js':f.ext==='.mts'?'.mjs':f.ext}'`)
export * as AddToCollectionButtonStories from '../components/atoms/AddToCollectionButton/AddToCollectionButton.stories.js'
export * as CollectionContributorCardStories from '../components/molecules/CollectionContributorCard/CollectionContributorCard.stories.js'
// @endindex
