// @index(['../components/**/*.stories.tsx'], f => `export * as ${f.name.replace('.stories','Stories')} from '${f.path}${f.ext==='.tsx'?'.js':f.ext==='.mts'?'.mjs':f.ext}'`)
export * as ResourceContributorCardStories from '../components/molecules/ResourceContributorCard/ResourceContributorCard.stories.js'
// @endindex
