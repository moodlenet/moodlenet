export { EdResourceEntitiesTools } from './entities.mjs'
export { Resource } from './init/sys-entities.mjs'
export {
  createResource,
  delResource,
  deltaResourcePopularityItem,
  patchResource,
  setResourceContent,
  setResourceImage,
} from './services.mjs'
export * from './types.mjs'
